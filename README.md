# RISC-Vduino UNO BLE
![输入图片说明](RISC-VduinoUNO%20BLE_%E4%BA%A7%E5%93%81%E7%85%A7%E7%89%87.jpg)
#### 介绍
   RISC-Vduino UNO BLE是由RISC-V单片机中文网（[RISC-V单片机中文社区]www.RISC-Vduino.cc）贡献者EngineerXu设计的一块开源硬件板，它是采用的是全球流行的RISC-V开源指令集设计的MCU，它可以使用自由和开源软件编写控制代码，RISC-Vduino UNO BLE是全球首个基于RISC-V开源指令集设计的低功耗蓝牙(Bluetooth Low Energy V4.2，V5.0)开源硬件板卡，RISC-Vduino UNO BLE和Arduino Wireless一样是面向创客的无线(Wireless MCU)开源硬件板卡，像Arduino Wireless Board一样进行有趣的智能家居，物联网电子，智能手环，智能手表DIY制作。 RISC-Vduino UNO BLE使用32位开源指令集MCU（eg,RISC-V），[过去的低功耗蓝牙](https://www.risc-v1.com/thread-2489-1-1.html)（Bluetooth Low Energy）大多使用8位或者32位AVR不开源指令集MCU（eg.8051，ARM），例如曾经BLE V4.0时代，很流行的CC2540经典的8051架构+BLE无线MCU；nRF51822采用32-bit ARM® Cortex™-M0 CPU；比如号称全球功率最低、体积最小的SmartBond-DA14580蓝牙智能系统级芯片(SoC),世界首款尺寸最小，功耗最小的BLE低功耗蓝牙芯片；还有STM32WB55采用超低功耗双核64MHz ARM® Cortex-M4+32MHz Cortex-M0+ MCU,BLE V5.2等等一些曾经流行的BLE SoC，具体可以参考如下[总结](https://www.risc-v1.com/thread-2489-1-1.html)。更多低功耗蓝牙的前世今生参见文章，回溯：[1999年~2022年蓝牙1.0技术发展历史](https://www.risc-v1.com/thread-2502-1-1.html)。
   目前RISC-V内核可编程芯片已经逐渐被广泛采用，基于RISC-V开源指令集设计，到开源硬件设计，再到开源软件系统等设计思路，正在逐步被大学学生和老师，工程师，单片机爱好者，物联网开发者等人们所支持。RISC-Vduino UNO BLE板子的微控制器包含Analog-GPIO/Digital-GPIO/PWM/DMA/ADC/SPI/UART/RS485可以用来处理更多的数学运算与通信控制，Arduino Wireless Board板子的微控制器通常采用5V作为工作电压，带来了电能消耗，功耗的增加； RISC-Vduino UNO BLE板子最低支持1.7V作为工作电压，更适合AIoT时代低功耗电子DIY制作，经济型和功能性更胜一筹。RISC-Vduino UNO BLE的可以运行在纽扣电池供电的环境，RISC-Vduino UNO BLE板子可以进入低功耗模式下，最小功耗大约为0.3uA~1.3uA，在当今节能减排，全球低碳排放的时代会更帮助您的无线电子产品，节能省电。蓝牙无线通信技术是在20多年时间一步一步走进我们的日常，带给我们便利的生活。相信RISC-V单片机中文网（[RISC-V单片机中文社区]www.RISC-Vduino.cc）出品的RISC-Vduino UNO BLE也能带给您便利的低功耗电子学习与DIY制作，比如如配合手机或者平板电脑，连接智能音箱或者智能手环等产品！

#### 软件架构
RISC-Vduino UNO BLE采用WCH的CH573 MCU，运行频率32MHz，软件支持寄存器操作或者库函数操作，同时也支持RTOS（eg.FreeRTOS，RT-Thread，TencentOS-Tiny，TMOS，......） 可以适配曾经Arduino Wireless Board扩展的传感器，执行器等硬件生态。

#### 安装教程

1.  安装电脑系统(Windows/Linux/Mac OS X/UOS)
2.  安装RISC-Vduino UNO BLE板子开发环境MounRiver Stuido IDE（也可以安装其他开发环境https://www.risc-v1.com/forum.php?gid=68）
3.  安装一个Arduino IDE串口助手用于串口监视RISC-Vduino DUE板子的运行情况和交互信息(也可以安装其他串口助手如RISC-Vduino串口助手，串口猎人，sscom32......)

#### 使用说明

1.  准备一台带有操作系统OS的 笔记本电脑PC 或者台式电脑PC 或者单板计算机SBC，保证其正常工作
2.  在电脑安装MounRiver Stuido IDE [http://www.mounriver.com/download] 或者Eclipse IDE [https://www.eclipse.org/downloads]
3.  在电脑安装一个串口助手工具如RISC-Vduino串口助手或者串口猎人或者Arduino IDE串口助手
4.  准备好RISC-Vduino UNO BLE板子
5.  准备好MiniUSB线，MicroUSB线，USB下载线，DC电源线
![输入图片说明](.gitee/RISC-V%E7%A4%BE%E5%8C%BA%E4%BA%A7%E5%93%81_RISC-Vduino%20UNO%20BLE.jpg)
6.  RISC-Vduino UNO BLE 连接智能手机蓝牙操作，低功耗蓝牙配对
![输入图片说明](RISC-VduinoUNOBLE_App%E8%BF%9E%E6%8E%A51.jpg)
![输入图片说明](RISCVduinoUNOble_App%E8%BF%9E%E6%8E%A51.jpg)
7.  RISC-Vduino UNO BLE 无线RSSI显示
![输入图片说明](CH573%20BLE%20%E6%89%8B%E6%9C%BA%E8%BF%9E%E6%8E%A5%E6%88%90%E5%8A%9F4.jpg)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
