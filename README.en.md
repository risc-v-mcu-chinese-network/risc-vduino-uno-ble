# RISC-Vduino UNO BLE
![输入图片说明](RISC-VduinoUNO%20BLE_%E4%BA%A7%E5%93%81%E7%85%A7%E7%89%87.jpg)
#### Description
RISC - Vduino UNO BLE is controlled by RISC -v Chinese website ([RISC - V microcontroller Chinese community] www.RISC-Vduino.cc) contributor EngineerXu design a piece of open source hardware board, it is to use an open source is the global epidemic of RISC - V instruction set design of MCU, it can control code, the use of free and open source software written RISC - Vduino UNO BLE is the world's first open source based on RISC - V instruction set design of Low power consumption Bluetooth (Bluetooth Low Energy V4.2, V5.0) open source hardware interface card.RISC - Vduino UNO BLE and Arduino as a guest oriented Wireless radio (Wireless MCU) open source hardware interface card, like the Arduino Wireless Board as an interesting intelligent household, electronic, the Internet of things intelligent hand ring, smart watch DIY production. RISC - Vduino UNO BLE to use 32-bit open-source instruction set MCU (eg, RISC - V), the Low power consumption Bluetooth (Bluetooth Low Energy) are mostly using 8 bit or 32 bit AVR is not open source instruction set MCU (eg. 8051, ARM).For example once BLE V4.0 era, popular CC2540 classic 8051 architecture + BLE wireless MCU; NRF51822 the 32 - bit ARM ® architecture ™ - M0 CPU; Such as the so-called global power has the lowest, the smallest SmartBond - DA14580 bluetooth intelligent system level chip (SoC), the world's first minimum size, low-power consumption minimum BLE bluetooth chips; And STM32WB55 ultra-low power consumption 64 MHZ ARM ® dual-core architecture (M4 + 32 MHZ architecture (M0 + MCU, BLE V5.2 and so on some popular BLE SoC, specific can refer to the following summary.More incarnations of low power consumption bluetooth see article, back: 1999 ~ 2022 bluetooth 1.0 technology development history. Currently RISC core -v programmable chip has gradually become widely adopted, based on RISC - V source instruction set design, to the open source hardware design, and then to open source software system design idea, is gradually being university students and teachers, engineers, single-chip computer enthusiasts, Internet of things developers and other support.RISC - Vduino UNO microcontrollers include Analog - BLE Board GPIO/Digital - GPIO/PWM/DMA/ADC/SPI/UART/RS485 and mathematical operations can be used to deal with more communication control, the Arduino Wireless Board, the Board of the micro controller usually use 5 v as working voltage, the power consumption, power consumption increase; RISC - Vduino UNO BLE board minimum support 1.7 V as working voltage, more suitable for low power electronic DIY AIoT era, economical and functional.RISC - Vduino UNO BLE can run in the environment of button batteries, RISC - Vduino UNO BLE boards can enter low power mode, the minimum power consumption is about 0.3 uA ~ 1.3 uA, in today's energy conservation and emissions reduction, the global low carbon era will be more help your wireless electronic products, energy saving province electricity. Bluetooth wireless communication technology is a step by step in 20 years time came into our daily, bring us convenient life. Believe RISC - V microcontroller Chinese website ([RISC - V microcontroller Chinese community] www.RISC-Vduino.cc) of RISC - Vduino UNO BLE can also bring you convenience low-power electronic learning and DIY production, such as cooperate with mobile phone or a tablet, intelligent speakers or intelligent hand ring and other products!

#### Software Architecture
RISC - Vduino UNO BLE the WCH CH573 MCU, 32 MHZ operating frequency, software support register or library function operation, at the same time also supports RTOS (eg. FreeRTOS, RT - Thread, TencentOS - Tiny, TMOS,... ) can fit once Arduino Wireless Board, extension of sensors, 

#### Installation

1.  Install the computer system (Windows/Linux/Mac OS X/UOS)
2.  Installation of RISC - Vduino UNO BLE board development environment MounRiver Stuido IDE (other development environment can also be installed at https://www.risc-v1.com/forum.php? Gid = 68)
3.  Install a Arduino IDE serial assistant for a serial port monitoring RISC - Vduino DUE to operation and mutual information of the board (can also be installed other serial assistants such as RISC - Vduino serial assistants, serial port hunter, sscom32... )

#### Instructions

1.  To prepare a computer with the operating system of the OS laptop PC or desktop PC or single board computer SBC, ensure the normal work
2.  In a computer installed MounRiver Stuido IDE [http://www.mounriver.com/download] or the Eclipse IDE [https://www.eclipse.org/downloads]
3.  In a serial computer installed helper tools such as RISC - Vduino a serial port or serial port hunter or Arduino IDE serial assistants
4.  Ready to RISC - Vduino UNO BLE boards
5.  Ready to MiniUSB line, MicroUSB line, USB download line, DC power supply cord
![输入图片说明](.gitee/RISC-V%E7%A4%BE%E5%8C%BA%E4%BA%A7%E5%93%81_RISC-Vduino%20UNO%20BLE.jpg)
#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
